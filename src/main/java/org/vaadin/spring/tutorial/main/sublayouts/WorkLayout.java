package org.vaadin.spring.tutorial.main.sublayouts;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
//import com.vaadin.ui.*;

public class WorkLayout extends VerticalLayout {

  Button uploadButton = new Button("Завантажити");
  Button validateButton = new Button("Валідувати");
  Button actualizeButton = new Button("Актуалізувати");

//  FormLayout uploadLayout = new FormLayout();

//  VerticalLayout buttonLayout = new VerticalLayout();

  public WorkLayout() {
//    setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
    setAlignItems(Alignment.CENTER);

//    uploadButton.setButtonCaption("Завантажити");

    Label topDirtyLabel = new Label();
    add(topDirtyLabel);
    topDirtyLabel.setSizeFull();
//    addComponentsAndExpand(topDirtyLabel); //todo

/*
    MultiFileMemoryBuffer buffer = new MultiFileMemoryBuffer();

    Upload upload = new Upload(buffer);
    upload.setUploadButton(uploadButton);
    upload.setDropAllowed(false);
*/

    add(uploadButton);
    add(validateButton);
    add(actualizeButton);
//    addComponent(uploadLupayout);
//    setUploads();

    String buttonWidth = "150px";   //todo: set this value by length of longest button
    uploadButton.setWidth(buttonWidth);
    validateButton.setWidth(buttonWidth);
    actualizeButton.setWidth(buttonWidth);

    validateButton.setEnabled(false);
    actualizeButton.setEnabled(false);

//    uploadButton.addStartedListener((Upload.StartedListener) startedEvent -> {
//      //todo save files
//      validateButton.setEnabled(true);
//    });

//    uploadButton.addClickListener((Button.ClickListener) clickEvent -> {
//      //todo save files
//      validateButton.setEnabled(true);
//    });

    uploadButton.addClickListener(buttonClickEvent -> {
      validateButton.setEnabled(true);
    });

//    validateButton.addClickListener((Button.ClickListener) clickEvent -> {
//      //todo: validate files
//      actualizeButton.setEnabled(true);
//    });

    validateButton.addClickListener(buttonClickEvent -> {
      actualizeButton.setEnabled(true);
    });


    //todo: add listener for actualize button


    Label bottomDirtyLabel = new Label();
//    addComponentsAndExpand(bottomDirtyLabel); //todo

//    addComponent(buttonLayout);

  }

//  void setUploads(){
//    UploadFinishedHandler handler = new UploadFinishedHandler() {
//      @Override
//      public void handleFile(InputStream inputStream, String s, String s1, long l, int i) {
//
//      }
//    };
//
////    UploadStateWindow window = new UploadStateWindow();
////
////    MultiFileUpload singleUpload = new MultiFileUpload(handler, window, false);
////    singleUpload.setCaption("Single upload");
////    singleUpload.setPanelCaption("Single upload(panel)");
////    singleUpload.getSmartUpload().setUploadButtonCaptions("Upload File", "");
////
////    MultiFileUpload multiUpload = new MultiFileUpload(handler, window);
////    multiUpload.setCaption("Multiple upload");
////    multiUpload.setPanelCaption("Multiple upload(panel)");
////    multiUpload.getSmartUpload().setUploadButtonCaptions("Upload File", "Upload Files");
////
////    uploadLayout.addComponent(singleUpload);
////    uploadLayout.addComponent(multiUpload);
//  }

//  String getMaxWith(Component ... buttons) {
//    return Arrays.stream(buttons)
//        .max(Comparator.comparingDouble(Component::getWidth)).get().getWidth() + "px";
//  }
}
