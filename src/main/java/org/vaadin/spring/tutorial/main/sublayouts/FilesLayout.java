package org.vaadin.spring.tutorial.main.sublayouts;

//import com.vaadin.ui.Label;
//import com.vaadin.ui.VerticalLayout;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class FilesLayout extends VerticalLayout {
  Label file1 = new Label("label 1");
  Label file2 = new Label("label 2");
  Label file3 = new Label("label 3");
  Label file4 = new Label("label 4");
  Label file5 = new Label("label 5");
  Label emptyFile = new Label();

  public FilesLayout(){
    add(file1);
    add(file2);
    add(file3);
//    addComponent(emptyFile);
//    setExpandRatio(emptyFile, 1);
    add(file4);
    add(file5);
//    addComponentsAndExpand(emptyFile); //todo

  }

  
}
