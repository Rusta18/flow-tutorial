package org.vaadin.spring.tutorial.main;

//import com.vaadin.annotations.Theme;
//import com.vaadin.annotations.Title;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.vaadin.spring.tutorial.main.sublayouts.ConsoleLayout;
import org.vaadin.spring.tutorial.main.sublayouts.FilesLayout;
import org.vaadin.spring.tutorial.main.sublayouts.WorkLayout;

//import com.vaadin.server.VaadinRequest;
//import com.vaadin.spring.annotation.SpringUI;
//import com.vaadin.ui.HorizontalLayout;
//import com.vaadin.ui.Label;
//import com.vaadin.ui.UI;
//import com.vaadin.ui.VerticalLayout;

//@Title("Test")  //todo
//@Theme("light") //todo
//@SpringUI(path = "/workspace")
@Route("workspace")
public class MainWorkspaceUI extends HorizontalLayout {

//  private String[] themes = { "valo", "light", "dark", "chameleon" };

//  private HorizontalLayout rootLayout;
  private VerticalLayout consoleAndWorkLayout = new VerticalLayout();
  private ConsoleLayout consoleLayout = new ConsoleLayout();
  private FilesLayout filesLayout = new FilesLayout();
  private WorkLayout workLayout = new WorkLayout();

//  @Override
//  protected void init(VaadinRequest request) {
//    ComboBox themePicker = new ComboBox("Theme", Arrays.asList(themes));
//    themePicker.setValue(getTheme());
//
//    themePicker.addValueChangeListener((HasValue.ValueChangeListener) event -> {
//      String theme = (String) event.getValue();
//      setTheme(theme);
//    });
//
//    setContent(themePicker);
//  }


//  @Override
  MainWorkspaceUI(/*VaadinRequest vaadinRequest*/) {
    consoleAndWorkLayout = new VerticalLayout();

    consoleAndWorkLayout.add(workLayout);
//    workLayout.setHeight("350px");

    consoleAndWorkLayout.add(consoleLayout);

//    rootLayout = new HorizontalLayout(consoleAndWorkLayout, filesLayout);
    add(consoleAndWorkLayout, filesLayout);

//    rootLayout.setSizeFull();
    setSizeFull();
//    rootLayout.setFlexGrow(5, consoleAndWorkLayout);
    setFlexGrow(5, consoleAndWorkLayout);
    consoleAndWorkLayout.setSizeFull();
//    rootLayout.setFlexGrow(2, filesLayout);
    setFlexGrow(2, filesLayout);
    filesLayout.setSizeFull();
    consoleAndWorkLayout.setFlexGrow(3, workLayout);
    workLayout.setSizeFull();
    consoleAndWorkLayout.setFlexGrow(1, consoleLayout);
    consoleLayout.setSizeFull();


    consoleLayout.setWidth("100%");
//    consoleAndWorkLayout.setHeight("400px");
//    rootLayout.setHeight();
//    rootLayout.setStyleName("backColor");
    discardMargins();
    testLayouts();
//    setContent(rootLayout);
  }

  void testLayouts() {
//    Button lightButton = new Button("light");
//    lightButton.setStyleName(ValoTheme.BUTTON_TINY);
//    workLayout.addComponent(lightButton);
//    Button darkButton = new Button("dark");
//    workLayout.addComponent(darkButton);
//    darkButton.setStyleName(ValoTheme.BUTTON_TINY);
//    Label dirtyLabel = new Label();
//    workLayout.addComponent(dirtyLabel);
//    workLayout.setExpandRatio(dirtyLabel, 1);

//    workLayout.setStyleName("work"); todo
//    consoleLayout.setStyleName("console"); todo
//    filesLayout.setStyleName("files"); todo
//    rootLayout.setStyleName("dark-work");
    consoleLayout.add(new Label("error"));

  }

  void discardMargins() {
//    consoleLayout.setMargin(false);
    workLayout.setMargin(false);
    consoleAndWorkLayout.setMargin(false);
//    filesLayout.setMargin(false);

//    rootLayout.setSpacing(false);
    setSpacing(false);
//    workLayout.setSpacing(false);
    consoleAndWorkLayout.setSpacing(false);
//    filesLayout.setSpacing(false);

//    rootLayout.setMargin(false);
  }


}
